#!/bin/bash
ERRORPATH="errors.txt"

if test -f $ERRORPATH; 
then
    rm $ERRORPATH
fi

for file in /log/*
do
  if [[ -f $file ]]; then
    cat $file | grep "status" >> $ERRORPATH
  fi
done