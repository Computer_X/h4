#!/bin/bash

. `dirname $0`/bashsimplecurses/simple_curses.sh

main (){
    #basic information, hostname, date, ...
    window "`hostname`" "red" "50%"
        append "`date`"
        addsep
        append_tabbed "Up since|`uptime | cut -f1 -d"," | sed 's/^ *//' | cut -f3- -d" "`" 2 "|"
        append_tabbed "`awk '{print "Load average:" $1 " " $2 " " $3}' < /proc/loadavg`" 2
    endwin

    #memory usage
    window "Memory usage" "red" "50%"
        append_tabbed `cat /proc/meminfo | awk '/MemTotal/ {print "Total:" $2/1024/1000}'` 2
        append_tabbed `cat /proc/meminfo | awk '/MemFree/ {print "Available:" $2/1024/1000}'` 2
    endwin

    #basic CPU info
    window "CPU" "green" "50%"
        append_tabbed "`cat /proc/cpuinfo | awk -F: '/^model name/{print "Model:" $2; exit}'`" 2
        append_tabbed "`cat /proc/cpuinfo | awk -F: '/^cpu MHz/{print "Speed MHz:" $2; exit}'`" 2
        append_tabbed "`cat /proc/cpuinfo | awk -F: '/^cpu cores/{print "Cores:" $2; exit}'`" 2
        append_tabbed "`cat /proc/cpuinfo | awk -F: '/^siblings/{print "Threads:" $2; exit}'`" 2
    endwin

    #basic disk info
    window "Disks" "yellow" "50%"
        disks=$(df -h | awk '{print $1 ":" $2 ":" $3 ":" $4}')
        for disk in $disks
        do
            append_tabbed "`echo $disk`" 5
        done
    endwin

        col_right
        move_up

    #5 most used processes ordered by cpu and memory usage
    window "Processes taking memory and CPU" "green" "50%"
    for i in `seq 2 6`; do
        append_tabbed "`ps ax -o pid,rss,pcpu,ucmd --sort=-cpu,-rss | sed -n "$i,$i p" | awk '{printf "%s: %smo:  %s%%" , $4, $2/1024, $3 }'`" 3
    done
    endwin

    #get dmesg, log it then send to deskbar
    window "Last kernel messages" "blue" "50%"
        dmesg | tail -n 10 > /dev/shm/deskbar.dmesg
        append_file /dev/shm/deskbar.dmesg
        rm -f /dev/shm/deskbar.dmesg
    endwin

    #a special manipulation to get net interfaces and IP
    window "Inet interfaces" "grey" "50%"
        _ifaces=$(for inet in `ifconfig | cut -f1 -d " " | sed -n "/./ p"`; do ifconfig $inet | awk 'BEGIN{printf "%s", "'"$inet"'"} /adr:/ {printf ":%s\n", $2}'|sed 's/adr://'; done)
        for ifac in $_ifaces; do
            append_tabbed  "$ifac" 4
        done
    endwin
}
main_loop 1