﻿open System
let EncryptString raw key (letters:string) =
    let rec Encrypt (raw:char List) key (letters:string) =
        match raw with
        | [] -> []
        | x::xs ->
            let lets = letters.IndexOf(x)

            if lets = -1 then
                [' '] @ Encrypt xs key letters
            else
                let newIndex = lets + key

                let final = 
                    if newIndex >= letters.Length then
                        newIndex - letters.Length
                    else
                        newIndex

                [letters.[final]] @ Encrypt xs key letters

    let data = Seq.toList(raw)
    let result = Encrypt data key letters

    String.Concat(Array.ofList(result))

let DecryptString raw key (letters:string) = 
    EncryptString raw (letters.Length - key) letters

[<EntryPoint>]
let main argv =
    let Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    let result = EncryptString "I AM NOICE" 4 Letters
    printfn "Result %A" result
    let Ha = DecryptString result 4 Letters
    printfn "Not secure %A" Ha
    0 // return an integer exit code
