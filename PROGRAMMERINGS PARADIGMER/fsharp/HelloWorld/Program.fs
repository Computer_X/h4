﻿// Learn more about F# at http://fsharp.org

open System

let WhoIsNoice person =
    printfn "%s is noice" person

[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"
    WhoIsNoice("Tobias")
    69 // return an integer exit code
