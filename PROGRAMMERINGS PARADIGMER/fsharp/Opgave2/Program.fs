﻿// Learn more about F# at http://fsharp.org

open System

let rec count = function
    | [] -> 0
    | x :: xs ->
        1 + count xs

[<EntryPoint>]
let main argv =
    let Mahlist = [5;8;2;45;9;3;1337]
    let count = count Mahlist
    printfn "Count: %i" count

    0 // return an integer exit code
