﻿// Learn more about F# at http://fsharp.org

open System

let rec reverse = function
    | [] -> []
    | x :: xs ->
        reverse xs @ [x]

[<EntryPoint>]
let main argv =
    let mahlist = [1; 2; 3; 4; 5; 6]
    let reversed = reverse mahlist
    printfn "%A" reversed
    
    0 // return an integer exit code
