﻿// Learn more about F# at http://fsharp.org

open System

let rec faku = function
    | 0 -> 1
    | x ->
        let next = x - 1
        x * faku next

let Betterfaku x =
    let rec faku x a =
        if x < 1 then
            a
        else
            let next = x - 1
            faku next (x * a)
    faku (x - 1) x

[<EntryPoint>]
let main argv =
    let num = 5
    let out = Betterfaku num
    printfn "Fak: %i" out
    0 // return an integer exit code
