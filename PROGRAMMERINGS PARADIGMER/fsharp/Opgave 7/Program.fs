﻿// Learn more about F# at http://fsharp.org

open System

let rec map list funcTodo = 
    match list with
    | [] -> []
    | x::xs ->
        [funcTodo x] @ map xs funcTodo

[<EntryPoint>]
let main argv =
    let result = map [5;10;15;0] ((*) 10)
    Console.WriteLine(result)
    0 // return an integer exit code
