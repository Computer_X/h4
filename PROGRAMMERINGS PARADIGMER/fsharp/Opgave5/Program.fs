﻿// Learn more about F# at http://fsharp.org

open System
        
let FindFibon x = 
    let rec fibon = function
        | index, lastValue, currentVal ->
            let newVal = lastValue + currentVal
            let newIndex = index - 1
            if newIndex < 2 then currentVal
            else fibon(newIndex, currentVal, newVal)
    fibon(x, 1
    , 1)

[<EntryPoint>]
let main argv =
    let result = FindFibon 8
    printfn "Result: %i" result
    0 // return an integer exit code
