﻿using System;

namespace VigCrypt
{
    public class Program
    {
        public static string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        static void Main(string[] args)
        {
            var key = "nicekey";

            var result = EncryptString("FUUUUUUUUUUUUCK you and also hi", key);
            Console.WriteLine(result);

            var ori = DecryptString(result, key);
            Console.WriteLine(ori);
        }

        public static string EncryptString(string message, string key)
        {
            Func<int, int, int> plus = (a, b) => a + b;

            return Encrypt(message, key, plus);
        }

        public static string DecryptString(string message, string key)
        {
            Func<int, int, int> plus = (a, b) => a - b;

            return Encrypt(message, key, plus);
        }

        private static char GetWrap(string str, int pos)
        {
            while (pos < 0)
            {
                pos += str.Length;
            }
            while (pos >= str.Length)
            {
                pos -= str.Length;
            }
            return str[pos];
        }

        private static string Encrypt(string message, string key, Func<int, int, int> fun)
        {
            message = message.ToUpper();
            key = key.ToUpper();

            var currentKeyIndex = 0;
            var output = "";

            foreach (var item in message)
            {
                if (item == ' ')
                {
                    output += ' ';
                    continue;
                }
                var index = Letters.IndexOf(item);
                var keyIndex = Letters.IndexOf(GetWrap(key, currentKeyIndex));

                output += GetWrap(Letters, fun(index, keyIndex));
                currentKeyIndex++;
            }

            return output;
        }
    }
}
