using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestCore
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test1()
        {
            var result = Encrypt_og_Decrypt.Program.EncryptString("succesful unit test", 5);
            Assert.AreEqual(result, "XZHHJXKZQ ZSNY YJXY");
        }

        [TestMethod]
        public void Test2()
        {
            var result = Encrypt_og_Decrypt.Program.EncryptString("finally done!", 13);
            Assert.AreEqual(result, "SVANYYL QBAR");
        }

        [TestMethod]
        public void Test3()
        {
            var result = Encrypt_og_Decrypt.Program.DecryptString("XZHHJXKZQ ZSNY YJXY", 5);
            Assert.AreEqual(result, "SUCCESFUL UNIT TEST");
        }

        [TestMethod]
        public void Test4()
        {
            var result = Encrypt_og_Decrypt.Program.DecryptString("SVANYYL QBAR", 13);
            Assert.AreEqual(result, "FINALLY DONE");
        }
    }
}
