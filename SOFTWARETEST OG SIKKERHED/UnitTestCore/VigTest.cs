﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestCore
{
    [TestClass]
    public class VigTest
    {
        [TestMethod]
        public void TestEncrypt()
        {
            var result = VigCrypt.Program.EncryptString("ANGRIBVEDDAGGRY", "VIGENERE");
            Assert.AreEqual(result, "VVMVVFMIYLGKTVP");
        }

        [TestMethod]
        public void TestDecrypt()
        {
            var result = VigCrypt.Program.DecryptString("VVMVVFMIYLGKTVP", "VIGENERE");
            Assert.AreEqual(result, "ANGRIBVEDDAGGRY");
        }
    }
}
