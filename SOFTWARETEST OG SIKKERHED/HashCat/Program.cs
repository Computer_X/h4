﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HashCat
{
    public class Program
    {
        static string FILEPATH = @"C:\Users\Compu\Desktop\secrets.txt";
        static void Main(string[] args)
        {
            while(true)
            {
                Console.Write("Enter username: ");
                var username = Console.ReadLine();
                Console.Write("Enter password: ");
                var password = Hash(Console.ReadLine(), username);
                if (checkPassword(username, password))
                {
                    break;
                }
            }

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Select option");
                Console.WriteLine("a = add user");
                Console.WriteLine("l = logout");

                var opt = Console.ReadKey(true).KeyChar;

                switch (opt)
                {
                    case 'a':
                        Console.Write("Enter username: ");
                        var newusername = Console.ReadLine();
                        Console.Write("Enter password: ");
                        var newpassword = Hash(Console.ReadLine(), newusername);
                        AddToFile(newusername, newpassword);
                        break;
                    case 'l':
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }

        public static bool checkPassword(string username, string password)
        {
            var users = GetUsers();

            try
            {
                var pass = users[username];
                if (pass == password)
                {
                    return true;
                }
            }
            catch (Exception) {  }
            Console.WriteLine("No access bitch");
            return false;
        }

        private static Dictionary<string, string> GetUsers()
        {
            var users = new Dictionary<string, string>();
            var fileData = File.ReadAllLines(FILEPATH);
            foreach (var item in fileData)
            {
                var data = item.Split(' ');
                users.Add(data[0], data[1]);
            }

            return users;
        }

        private static void AddToFile(string username, string password)
        {
            File.AppendAllText(FILEPATH, username + " " + password + "\n");
        }

        public static string Hash(string password, string salt)
        {
            byte[] plainText = Encoding.UTF8.GetBytes(password);
            byte[] saltArray = Encoding.UTF8.GetBytes(salt);
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[plainText.Length + saltArray.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < saltArray.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = saltArray[i];
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }
    }
}
