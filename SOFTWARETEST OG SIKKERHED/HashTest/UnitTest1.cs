using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HashTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checkPassword()
        {
            var result = HashCat.Program.checkPassword("steff", "2jU66vd6peRMUnIYIOZnRtegJEK18x8kbwU66XeW988=");
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void Hash()
        {
            var result = HashCat.Program.Hash("123qwe123", "steff");
            Assert.AreEqual("2jU66vd6peRMUnIYIOZnRtegJEK18x8kbwU66XeW988=", result);
        }
        [TestMethod]
        public void Hash2()
        {
            var result = HashCat.Program.Hash("123qwe123", "brix");
            Assert.AreEqual("MKxJ057WNcb4ol8m3ioQig7/kG3C7szLoRA4ydJphsM=", result);
        }
    }
}
