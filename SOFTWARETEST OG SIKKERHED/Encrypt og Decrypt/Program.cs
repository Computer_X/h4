﻿using System;
using System.Threading;
using System.IO;
using System.Collections.Generic;

namespace Encrypt_og_Decrypt
{
    public class Program
    {
        public static string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static string AllowdLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

        static void Main(string[] args)
        {
            int key = 7;

            //Console.WriteLine("Write your string bitch");
            //var raw = Console.ReadLine();

            Console.WriteLine("Reading file...");

            var raw = File.ReadAllLines(@"C:\Users\Steff\Desktop\fuck yeah.txt");
            var str = "";
            foreach (var item in raw)
            {
                str += item + " ";
            }

            Console.WriteLine("Got: " + str);

            var encrypt = EncryptString(str, key);
            Console.WriteLine("Much secure " + encrypt);

            Console.WriteLine("Saving...");

            File.WriteAllLines(@"C:\Users\Steff\Desktop\who knows.txt", new List<string> { encrypt });


            Thread.Sleep(1000);

            Console.WriteLine("THE TIME HAS COME");
            Console.WriteLine("TO DECRYPT");
            var decrypt = DecryptString(encrypt, key);
            Console.WriteLine("Messege: " + decrypt);
        }

        public static string FixString(string raw)
        {
            var newstr = raw.ToUpper();
            foreach (var item in newstr)
            {
                if (!AllowdLetters.Contains(item))
                {
                    newstr = newstr.Replace(item.ToString(), "");
                }
            }
            return newstr;
        }

        public static string EncryptString(string raw, int key)
        {
            var output = "";
            foreach (var item in FixString(raw))
            {
                if (item == ' ')
                {
                    output += item;
                    continue;
                }
                var index = Letters.IndexOf(item);

                var newIndex = index + key;

                // Loop around
                if (newIndex >= Letters.Length)
                {
                    newIndex -= Letters.Length;
                }

                output += Letters[newIndex];
            }
            return output;
        }

        public static string DecryptString(string raw, int key)
        {
            var output = "";
            foreach (var item in FixString(raw))
            {
                if (item == ' ')
                {
                    output += item;
                    continue;
                }
                var index = Letters.IndexOf(item);

                var newIndex = index - key;

                // Loop around
                if (newIndex < 0)
                {
                    newIndex += Letters.Length;
                }

                output += Letters[newIndex];
            }
            return output;
        }
    }
}
