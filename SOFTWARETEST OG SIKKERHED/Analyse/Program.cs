﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Analyse
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"C:\Users\Steff\Desktop\who knows Tobias Steffensen.txt";
            Console.WriteLine("Reading " + path);
            var data = File.ReadAllLines(path);
            var wordList = File.ReadAllLines(@"C:\Users\Steff\Desktop\wordList.txt");

            var betterWordList = new List<string>();
            foreach (var item in wordList)
            {
                if (item.Length > 4)
                {
                    betterWordList.Add(item.ToUpper());
                }
            }

            //CrappyCracker(data, betterWordList);
            BetterCracker(data, betterWordList);
        }

        private static void BetterCracker(string[] data, List<string> betterWordList)
        {
            var text = data[0];

            var letters = Encrypt_og_Decrypt.Program.Letters;

            Dictionary<char, double> scores = new Dictionary<char, double>();

            foreach (var letter in letters)
            {
                scores.Add(letter, 0);
                foreach (var item in text)
                {
                    if (letter == item)
                    {
                        scores[item]++;
                    }
                }
            }

            var highest = ' ';

            foreach (var item in scores)
            {
                if (item.Value > highest)
                {
                    highest = item.Key;
                }
            }

            var diff = 0;

            for (int i = 4; i < letters.Length; i++)
            {
                if (letters[i] == highest)
                {
                    break;
                }
                diff++;
            }

            var encryptTest = Encrypt_og_Decrypt.Program.DecryptString(data[0], diff);
            Console.WriteLine("Result " + encryptTest);
        }

        private static void CrappyCracker(string[] data, List<string> betterWordList)
        {
            var final = "";
            var letters = Encrypt_og_Decrypt.Program.Letters;
            for (int i = 0; i < letters.Length; i++)
            {
                if (final != "")
                {
                    break;
                }
                Console.WriteLine("Trying: " + i.ToString());
                var encryptTest = Encrypt_og_Decrypt.Program.DecryptString(data[0], i);

                foreach (var item in betterWordList)
                {
                    if (encryptTest.Contains(item))
                    {
                        Console.WriteLine("With word: " + item);
                        Console.WriteLine("Found key: " + i.ToString());
                        Console.WriteLine("Result: " + encryptTest);
                        final = encryptTest;
                        break;
                    }
                }
            }
        }
    }
}
