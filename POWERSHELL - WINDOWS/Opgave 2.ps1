$valuesToLookFor = @(".ps1",".cmd")

"Getting files ..."
$data = Get-ChildItem -Recurse -Path "C:\" | Where-Object {$valuesToLookFor -contains $_.Extension}
"Files count:"
($data | Measure-Object).Count