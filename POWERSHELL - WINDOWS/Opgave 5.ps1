$temp = $env:LOCALAPPDATA + "\Temp"

([Math]::Round(((Get-ChildItem $temp -Recurse | Measure-Object -Property Length -Sum -ErrorAction Stop).Sum / 1MB))).ToString() + " MB in temp folder"

$ans = Read-Host -Prompt "Do you want to delete it?"
if ($ans.ToLower()[0] -eq 'y') {
    Get-ChildItem -Path $temp -Include *.* -File -Recurse | ForEach-Object { 
        try {
            $_.Delete()
        }
        catch { "An error occurred." }
    }

}