$From = "tsminecraftletsplay@gmail.com"
$To = "tsminecraftletsplay@gmail.com"
$Subject = "WE HAVE A PROBLEM"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"

$warnings = Get-EventLog -Newest 10 -LogName "system" | Where-Object {($_.EntryType -Match "Error") -or ($_.EntryType -Match "Warning")};
$warnings

if ($warnings.Count -eq 0) {
    "No warnings... for now"
}
else {
    $Body = "";

    ForEach ($warn in $warnings) {
        $Body += "Index`t" + $warn.Index.ToString() + "`r`n"
        $Body += "Time`t" + $warn.TimeGenerated.ToString() + "`r`n"
        $Body += "EntryType`t" + $warn.EntryType.ToString() + "`r`n"
        $Body += "Source`t" + $warn.Source.ToString() + "`r`n"
        $Body += "InstanceID`t" + $warn.InstanceID.ToString() + "`r`n"
        $Body += "Message`t" + $warn.Message.ToString()

        $Body += "`r`n`r`n ----------------------------------------------------------------- `r`n`r`n"
    }
    
    Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -UseSsl -Credential (Get-Credential)
}