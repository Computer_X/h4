$disk = Get-WmiObject Win32_LogicalDisk -Filter ("DeviceID='C:'") | Select-Object Size,FreeSpace
$CPUInfo = Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number* 

function getCPU {
    $proc = Get-WmiObject Win32_Processor
    return $proc.LoadPercentage.ToString() + "%"
}

function getRAM {
    $ram = Get-Ciminstance Win32_OperatingSystem
    return ([Math]::Round($ram.FreePhysicalMemory / 1MB)).ToString() + "GB"
}

function getTotalRAM {
    return ([Math]::Round((Get-WmiObject -Class Win32_ComputerSystem).TotalPhysicalMemory/1GB)).ToString() + "GB"
}

function getDiskSize {
    return ([Math]::Round($disk.Size / 1GB)).ToString() + "GB"
}

function getDiskFree {
    return ([Math]::Round($disk.FreeSpace / 1GB)).ToString() + "GB"
}