$temp = $env:LOCALAPPDATA + "\Temp"

function getTempFileCount {
    $data = Get-ChildItem -Recurse -Path $temp -ErrorAction SilentlyContinue
    return $data.Count
}

getTempFileCount