# Create window
Add-Type -AssemblyName PresentationFramework, System.Windows.Forms
[xml]$xaml = Get-Content "ui.xml"
$script:hash = [hashtable]::Synchronized(@{})
$hash.Window = [Windows.Markup.XamlReader]::Load((New-Object -TypeName System.Xml.XmlNodeReader -ArgumentList $xaml))
$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object -Process {
    $hash.$($_.Name) = $hash.Window.FindName($_.Name)
}
# Find elements
$BTN_Update = $hash.Window.FindName("BTN_Update")
$LB_Events = $hash.Window.FindName("LB_Events")
$TB_TotalMemory = $hash.Window.FindName("TB_TotalMemory")
$TB_TotalCPU = $hash.Window.FindName("TB_TotalCPU")
$TB_TotalCPUThread = $hash.Window.FindName("TB_TotalCPUThread")
$TB_MemoryFree = $hash.Window.FindName("TB_MemoryFree")
$TB_DiskSize = $hash.Window.FindName("TB_DiskSize")
$TB_FreeDiskSize = $hash.Window.FindName("TB_FreeDiskSize")
$TB_CPULoad = $hash.Window.FindName("TB_CPULoad")
$TB_Services = $hash.Window.FindName("TB_Services")
$LB_Specs = $hash.Window.FindName("LB_Specs")
$LB_Memory = $hash.Window.FindName("LB_Memory")
$SP_CPUAndDrives = $hash.Window.FindName("SP_CPUAndDrives")

# import functions
. "./event-boi.ps1"
. "./resource-boi.ps1"
. "./spec-boi.ps1"

# Setup
$BTN_Update.Add_Click({
    Update
})

function Update {
    $TB_TotalMemory.Text = getTotalRAM
    $TB_TotalCPU.Text = $CPUInfo.NumberOfCores
    $TB_TotalCPUThread.Text = $CPUInfo.NumberOfLogicalProcessors
    $TB_MemoryFree.Text = getRAM
    $TB_DiskSize.Text = getDiskSize
    $TB_FreeDiskSize.Text = getDiskFree
    $TB_CPULoad.Text = getCPU
    $TB_Services.Text = getServices

    addSpecs $LB_Specs
    addSpecsMemory $LB_Memory
    addDrives $SP_CPUAndDrives

    $LB_Events.Items.Clear()
    $events = getEvents
    foreach ($item in $events) {
        $LB_Events.Items.Add($item.Message)
    }
}

function MessageBox {
    param (
        $Message
    )
    [System.Windows.MessageBox]::Show($Message)
}

# Dont work
#$job = Start-Job -FilePath "./temp-boi.ps1"
#$jobEvent = Register-ObjectEvent $job StateChanged -Action {
#    Write-Host ('Job #{0} ({1}) complete.' -f $sender.Id, $sender.Name)
#
#    $data = Receive-Job -job $job
#    $LB_Specs.Items.Add($data)
#
#    $jobEvent | Unregister-Event
#}
    
# Show window
Update
$null = $hash.Window.ShowDialog()