function addSpecs {
    param (
        $ListBox
    )
    $ListBox.Items.Clear()
    $pro = Get-WmiObject Win32_Processor
    $ListBox.Items.Add($pro.PSComputerName)
    $ListBox.Items.Add("")
    $ListBox.Items.Add($pro.Manufacturer)
    $ListBox.Items.Add($pro.Name)
    $ListBox.Items.Add("ClockSpeed: " + ($pro.MaxClockSpeed / 1000).ToString() + "GHz")
}

function addSpecsMemory {
    param (
        $ListBox
    )
    $ListBox.Items.Clear()
    $memory = Get-WmiObject win32_physicalmemory
    
    $ListBox.Items.Add("Memory:")
    $ListBox.Items.Add("")
    foreach ($mem in $memory) {
        $ListBox.Items.Add($mem.DeviceLocator)
        $ListBox.Items.Add($mem.Manufacturer + " " + ([Math]::Round($mem.Capacity / 1GB)).ToString() + "GB")
        $ListBox.Items.Add("ClockSpeed: " + ($mem.ConfiguredClockSpeed).ToString() + "MHz")
        $ListBox.Items.Add("")
    }
}

function addDrives {
    param (
        $StackPanel
    )
    $StackPanel.Children.Clear();
    $disks = Get-WmiObject Win32_LogicalDisk
    
    foreach ($disk in $disks) {
        "Childs:"

        $textBlock = New-Object System.Windows.Controls.TextBlock
        $textBlock.Text = $disk.VolumeName + "`r`n"
        $textBlock.Text += $disk.DeviceID + " " + ([Math]::Round($disk.Size / 1GB)).ToString() + "GB`r`n"
        $textBlock.Text += "Free space: " + ([Math]::Round($disk.FreeSpace / 1GB)).ToString() + "GB`r`n"
        $textBlock.Text += $disk.FileSystem
        
        $fullness = New-Object System.Windows.Controls.ProgressBar
        $fullness.Height = 15
        $fullness.Maximum = $disk.Size
        $fullness.Minimum = 0
        $fullness.Value = $disk.Size - $disk.FreeSpace 
        
        $SP_CPUAndDrives.AddChild($textBlock)
        $SP_CPUAndDrives.AddChild($fullness)
        $space = New-Object System.Windows.Controls.TextBlock
        $SP_CPUAndDrives.AddChild($space)
    }
}