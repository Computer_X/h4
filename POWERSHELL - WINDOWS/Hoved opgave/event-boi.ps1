function getEvents {
    return Get-EventLog -Newest 1000 -LogName "system" | Where-Object {($_.EntryType -Match "Error") -or ($_.EntryType -Match "Warning")} | Select-Object -first 10
}

function getServices {
    $serv = Get-Service | Where-Object {$_.Status -eq "Running"}
    return $serv.Count
}