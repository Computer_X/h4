$drives = (Get-PSDrive).Name -match '^[a-z]$'
"Drives:"
$drives

foreach ($item in $drives) {
    $driveLet = $item

    "Scanning drive " + $driveLet + " ..."
    $data = Get-ChildItem -Recurse -Path  ($driveLet + ":\") -ErrorAction SilentlyContinue
    "Files count:"
    ($data | Measure-Object).Count

    $disk = Get-WmiObject Win32_LogicalDisk -Filter ("DeviceID='"+ $driveLet +":'") |
    Select-Object Size,FreeSpace
    "Size"
    ([Math]::Round($disk.Size / 1GB)).ToString() + " GB"
    "Free"
    ([Math]::Round($disk.FreeSpace / 1GB)).ToString() + " GB"
    "Biggest files:"
    $data | Sort-Object -descending -property length | Select-Object -first 5 name, length
}