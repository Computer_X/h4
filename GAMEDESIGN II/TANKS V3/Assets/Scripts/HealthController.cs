﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public float Health = 5;
    public GameObject attacker;
    public Slider HealthBar;
    public GameObject HealthCanvas;
    public float DisplayRange = 60;

    private float Maxhealth;
    private Transform cam;
    private GameObject Player;

    private void Start()
    {
        var play = FindObjectOfType<PlayerController>();
        if (play != null)
        {
            Player = play.gameObject;
        }
        Maxhealth = Health;
        cam = Camera.main.transform;
    }

    private void Update()
    {
        if (HealthCanvas != null)
        {
            HealthCanvas.SetActive(Vector3.Distance(Player.transform.position, transform.position) < DisplayRange);
            
            HealthCanvas.transform.rotation = cam.rotation;
        }
    }

    public void TakeDamage(int dmg)
    {
        Health -= dmg;
        if (HealthBar != null)
        {
            HealthBar.value = Health / Maxhealth;
        }

        if (Health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if (gameObject.tag == "player")
        {
            FindObjectOfType<GameManager>().Restart();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}