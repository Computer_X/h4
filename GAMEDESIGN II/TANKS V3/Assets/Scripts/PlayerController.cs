﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed;
    public float TurnSpeed;
    public Transform cam;
    public Transform Barrel;
    public float BarrelSmoothTime;

    private Rigidbody TankRigidbody;
    private GunController GunController;

    // Start is called before the first frame update
    void Start()
    {
        TankRigidbody = GetComponent<Rigidbody>();
        GunController = Barrel.GetComponent<GunController>();
    }

    void FixedUpdate()
    {
        float Forward = Input.GetAxis("Vertical");
        Vector3 movement = transform.forward * Forward * Speed * Time.deltaTime;

        TankRigidbody.MovePosition(TankRigidbody.position + movement);

        float turn = Input.GetAxis("Horizontal") * TurnSpeed * Time.deltaTime;
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        TankRigidbody.MoveRotation(TankRigidbody.rotation * turnRotation);


        Barrel.rotation = Quaternion.Slerp(Barrel.rotation, Quaternion.Euler(0, cam.eulerAngles.y, 0), BarrelSmoothTime);

        if (Input.GetAxis("Fire1") != 0)
        {
            GunController.Shot();
        }
    }

}
