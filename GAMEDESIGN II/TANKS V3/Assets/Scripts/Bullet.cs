﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public List<string> CanHit;
    public int damage = 1;

    private void OnTriggerEnter(Collider other)
    {
        GameObject target = other.gameObject;
        Log("Hit: " + target.tag);

        if (CanHit.Contains(target.tag))
        {
            Log("GOT EM");
            target.GetComponent<HealthController>().TakeDamage(damage);
            Destroy(this.gameObject);
        }
    }

    private void Log(string message)
    {
        Debug.Log("Bullet: " + message);
    }
}
