﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public GameObject Bullet;
    public Transform BulletSpawn;
    public float BulletSpeed;
    public float ReloadTime;
    public List<string> CanHitTags;

    private bool firing;
    private float OrginalFireRate;
    private Rigidbody ParrentBody;

    private void Start()
    {
        ParrentBody = transform.parent.GetComponent<Rigidbody>();
    }

    public void Shot()
    {
        if (firing == false)
        {
            firing = true;
            FireBullet();
        }
    }

    private void FireBullet()
    {
        GameObject Bul = Instantiate(Bullet, BulletSpawn.transform.position, BulletSpawn.rotation);
        Bul.GetComponent<Bullet>().CanHit = CanHitTags;

        Vector3 vec = transform.forward * BulletSpeed;
        Bul.GetComponent<Rigidbody>().AddForce(vec, ForceMode.Impulse);
        ParrentBody.AddForce(-(vec / 2), ForceMode.Impulse);
        Destroy(Bul, ReloadTime);


        Invoke("Reload", ReloadTime);
    }

    private void Reload()
    {
        firing = false;
    }

    public void RapidFire(float FireRate, float Duration)
    {
        OrginalFireRate = ReloadTime;

        ReloadTime = FireRate;
        Invoke("ResetFirerate", Duration);
        Debug.Log("RapidFire Activated orginal firerate is " + OrginalFireRate);
    }

    private void ResetFirerate()
    {
        ReloadTime = OrginalFireRate;
        Debug.Log("RapidFire DeActivated");
    }

}
