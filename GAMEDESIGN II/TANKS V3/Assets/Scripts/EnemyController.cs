﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FluentBehaviourTree;
using System;

public class EnemyController : MonoBehaviour
{
    public float Range;
    public float ShootingRange;
    public float StopDistance;
    public float BarrelSmoothTime;
    public GameObject Turret;

    private NavMeshAgent Agent;
    private GunController GunController;

    private GameObject Player;

    float DistanceToTarget
    { 
        get 
        { 
            return Vector3.Distance(Target.transform.position, transform.position);
        } 
    }

    public GameObject Target;

    private IBehaviourTreeNode tree;

    // Start is called before the first frame update
    void Start()
    {
        GunController = Turret.GetComponent<GunController>();
        Agent = GetComponent<NavMeshAgent>();
        var play = FindObjectOfType<PlayerController>();

        if (play != null)
        {
            Player = play.gameObject;
        }

        BehaviourTreeBuilder builder = new BehaviourTreeBuilder();
        tree = builder.Sequence("root")
            .Selector("Check target")
                .Sequence("check player")
                    .Do("Is player close", isPlayerClose)
                    .Do("set player as target", setPlayerTarget)
                    .End()
                .Do("Is target null", isTargetSet)
                .Do("Find house", setHouseTarget)
                .End()

            .Selector("Move")
                .Sequence("Is target too close")
                    .Do("Is target too close", isTargetToClose)
                    .Do("Stop moving", stopMoving)
                    .End()
                .Sequence("Is target too far")
                    .Do("Is target far", isTargetToFar)
                    .Do("Find house", setHouseTarget)
                    .End()
                .Do("Move to target ", goToTarget)
                .End()

            .Sequence("can shoot")
                .Do("Is target close", targetWithindRange)
                .Do("Shot at target", shotTarget)
                .End()
            .End()
            .Build();
    }

    // Update is called once per frame
    void Update()
    {
        TimeData time = new TimeData(Time.deltaTime);
        tree.Tick(time);

        if (Target != null)
        {
            Quaternion rotation = Quaternion.LookRotation(Target.transform.position - transform.position, transform.TransformDirection(Vector3.up));

            Turret.transform.rotation = Quaternion.Slerp(
                Turret.transform.rotation,
                Quaternion.Euler(0, rotation.eulerAngles.y, 0),
                BarrelSmoothTime);
        }
    }

    private BehaviourTreeStatus stopMoving(TimeData arg)
    {
        Agent.isStopped = true;
        return BehaviourTreeStatus.Success;
    }

    private BehaviourTreeStatus isTargetSet(TimeData arg)
    {
        if (Target != null)
        {
            return BehaviourTreeStatus.Success;
        }
        else
        {
            return BehaviourTreeStatus.Failure;
        }
    }

    private BehaviourTreeStatus isTargetToClose(TimeData arg)
    {
        if (Target == null)
        {
            return BehaviourTreeStatus.Failure;
        }

        if (DistanceToTarget < StopDistance)
        {
            Agent.isStopped = true;
            return BehaviourTreeStatus.Success;
        }
        else
        {
            Agent.isStopped = false;
            return BehaviourTreeStatus.Failure;
        }
    }

    private BehaviourTreeStatus targetWithindRange(TimeData arg)
    {
        if (DistanceToTarget < ShootingRange)
        {
            return BehaviourTreeStatus.Success;
        }
        else
        {
            return BehaviourTreeStatus.Failure;
        }
    }

    private BehaviourTreeStatus shotTarget(TimeData arg)
    {
        //Debug.Log("SHOT HIM");

        GunController.Shot();
        return BehaviourTreeStatus.Success;
    }

    private BehaviourTreeStatus isPlayerClose(TimeData arg)
    {
        if (Player != null)
        {
            if (Vector3.Distance(Player.transform.position, transform.position) <= Range)
            {
                //Debug.Log("Player is close. go get em");
                return BehaviourTreeStatus.Success;
            }
            else
            {
                return BehaviourTreeStatus.Failure;
            }
        }
        else
        {
            return BehaviourTreeStatus.Failure;

        }
    }

    private BehaviourTreeStatus isTargetToFar(TimeData arg)
    {
        if (Vector3.Distance(Target.transform.position, transform.position) > Range + 5)
        {
            Target.GetComponent<HealthController>().attacker = null;
            Target = null;
            return BehaviourTreeStatus.Success;
        }
        else
        {
            return BehaviourTreeStatus.Failure;
        }
    }

    private BehaviourTreeStatus setPlayerTarget(TimeData arg)
    {
        //Log("Found player");
        var health = Player.GetComponent<HealthController>();
        if (health.attacker == null)
        {
            Target = Player;
            health.attacker = gameObject;
        }
        return BehaviourTreeStatus.Success;
    }

    private BehaviourTreeStatus setHouseTarget(TimeData arg)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Range);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.gameObject.tag == "house")
            {
                var health = hitCollider.gameObject.GetComponent<HealthController>();
                if (health.attacker == null)
                {
                    Log("Found house");
                    Target = hitCollider.gameObject;
                    health.attacker = gameObject;
                    return BehaviourTreeStatus.Success;
                }
            }
        }

        return BehaviourTreeStatus.Failure;
    }

    private BehaviourTreeStatus goToTarget(TimeData arg)
    {

        if (Target != null)
        {
            Agent.destination = Target.transform.position;
            return BehaviourTreeStatus.Success;
        }
        return BehaviourTreeStatus.Failure;
    }

    private void Log(string message)
    {
        Debug.Log("EnemyController: " + message);
    }
}
